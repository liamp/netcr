local Game = {};

local TEAM_BLUE = 1;
local TEAM_RED  = 0;
local TEAM_NONE = -1;

local CurrentPlayer = TEAM_RED;
local Winner = TEAM_NONE;
local Over = false;

local Score = {};
Score[TEAM_RED]  = 0;
Score[TEAM_BLUE] = 0;

local FirstTurn = {};
FirstTurn[TEAM_RED]  = true;
FirstTurn[TEAM_BLUE] = true;

local Field = nil
local FieldRows;
local FieldCols;

-- GetAdjacent {{{
local function GetAdjacent(R, C)
    local Neighbours = {}
    local Index = 1

    if ((R - 1 > 0) and (R - 1 <= FieldRows)) then
        Neighbours[Index] = { Row = (R - 1), Col = C }
        Index = Index + 1
    end

    if ((R + 1 > 0) and (R + 1 <= FieldRows)) then
        Neighbours[Index] = { Row = (R + 1), Col = C } 
        Index = Index + 1
    end

    if ((C - 1 > 0) and (C - 1 <= FieldCols)) then
        Neighbours[Index] = { Row = R, Col = (C - 1) }
        Index = Index + 1
    end

    if ((C + 1 > 0) and (C + 1 <= FieldCols)) then
        Neighbours[Index] = { Row = R, Col = (C + 1) }
        Index = Index + 1
    end

    return Neighbours
end
-- }}}

-- UpdateScore {{{
local function UpdateScore()
    Score[TEAM_RED] = 0;
    Score[TEAM_BLUE] = 0;

    for R = 1, FieldRows do
        for C = 1, FieldCols  do
            local Cell = Field[R][C];
            if (Cell.Owner ~= TEAM_NONE) then
                Score[Cell.Owner] = Score[Cell.Owner] + Cell.Atoms;
            end
        end
    end

    if (not (FirstTurn[TEAM_RED] or FirstTurn[TEAM_BLUE])) then
        if (Score[TEAM_RED] == 0) then
            Game.Winner = TEAM_BLUE;
            Game.Over = true;
        end

        if (Score[TEAM_BLUE] == 0) then
            Game.Winner = TEAM_RED;
            Game.Over = true;
        end
    end
end
-- }}}

function Game.Init(Rows, Cols)
    Field = {};
    FieldRows = Rows;
    FieldCols = Cols;

    for R = 1, Rows do
        Field[R] = {};
        for C = 1, Cols do
            local CellNeighbours = GetAdjacent(R, C);
            local CellCapacity = (#CellNeighbours - 1);

            Field[R][C] = {
                Owner = TEAM_NONE,
                Capacity = CellCapacity,
                Neighbours = CellNeighbours,
                Atoms = 0,
            };

        end
    end
end


function Game.PlaceAtom(R, C, Team)
    local Cell = Field[R][C];

    if (Cell.Owner == Team or Cell.Owner == TEAM_NONE) then
        if (FirstTurn[Team]) then FirstTurn[Team] = false end;

        if (Cell.Atoms == Cell.Capacity) then
            -- Explode cell
            Cell.Owner = TEAM_NONE;
            Cell.Atoms = 0;

            for N = 1, #Cell.Neighbours do
                local AdjRow = Cell.Neighbours[N].Row;
                local AdjCol = Cell.Neighbours[N].Col;
                Game.PlaceAtom(AdjRow, AdjCol);
            end
        else
            Cell.Owner = Team;
            Cell.Atoms = Cell.Atoms + 1;
        end

        return true;
    end

    return false;
end

function Game.PlaceAtomForCurrentPlayer(R, C)
    local Cell = Field[R][C];

    if (FirstTurn[CurrentPlayer]) then 
        FirstTurn[CurrentPlayer] = false 
    end

    if (Cell.Atoms == Cell.Capacity) then
        -- Explode cell
        Cell.Owner = TEAM_NONE;
        Cell.Atoms = 0;

        for N = 1, #Cell.Neighbours do
            local AdjRow = Cell.Neighbours[N].Row;
            local AdjCol = Cell.Neighbours[N].Col;
            Game.PlaceAtomForCurrentPlayer(AdjRow, AdjCol);
        end
    else
        Cell.Owner = CurrentPlayer;
        Cell.Atoms = Cell.Atoms + 1;
    end

    return true;

end

function Game.EndTurnForCurrentPlayer()
    CurrentPlayer = 1 - CurrentPlayer;
    UpdateScore();
end

function Game.GetCellAt(R, C)
    return Field[R][C];
end

function Game.CurrentPlayer()
    return CurrentPlayer;
end

Game.TEAM_RED = TEAM_RED;
Game.TEAM_BLUE = TEAM_BLUE;
Game.TEAM_NONE = TEAM_NONE;
Game.Score = Score;

return Game;
