local socket = require "socket";
local game   = require "game";

local FIELD_ROWS = 5
local FIELD_COLS = 6
local CELL_WIDTH = 120
local CELL_HEIGHT = 120
local FIELD_WIDTH = FIELD_COLS * CELL_WIDTH
local FIELD_HEIGHT = FIELD_ROWS * CELL_HEIGHT
local OFFSET_X = (1280 - (FIELD_COLS * CELL_WIDTH)) / 2
local OFFSET_Y = (720 - (FIELD_ROWS * CELL_HEIGHT)) / 2
local DEFAULT_COLOR = { 127, 127, 127, 255 }

local SCREEN_CENTER_X = 640;
local SCREEN_CENTER_Y = 360;

local TEAM_RED = game.TEAM_RED;
local TEAM_BLUE = game.TEAM_BLUE;
local TEAM_NONE = game.TEAM_NONE;


local PORT = 12345

local Colors = {};
Colors[TEAM_BLUE] = { 0, 0, 127, 127 };
Colors[TEAM_RED] =  { 127, 0, 0, 127 };
Colors[TEAM_NONE] = { 0, 0, 0, 0 }; 


local AtomOffsets = {
    { X = CELL_WIDTH * 0.25, Y = CELL_HEIGHT * 0.25 },
    { X = CELL_WIDTH * 0.75, Y = CELL_HEIGHT * 0.25 },
    { X = CELL_WIDTH * 0.75, Y = CELL_HEIGHT * 0.75 },
    { X = CELL_WIDTH * 0.25, Y = CELL_HEIGHT * 0.75 }
}

local Connection = nil
local Host = false

local OpponentIP = nil;
local OpponentPort = nil;

local MyTeam = nil;

local OpponentFound = false;

local CellPositions = nil;

-- CheckForOpponent {{{
local function CheckForOpponent()
    local Data = nil;
    if (Host) then
        Data,OpponentIP, OpponentPort = Connection:receivefrom();

        if (Data) then
            Connection:sendto(Data, OpponentIP, OpponentPort);
            return true;
        end
    else
        Data = Connection:receive();
        if (Data) then
            return true;
        end
    end

    return false;
end
-- }}}

-- DrawAtomsForCell {{{
local function DrawAtomsForCell(Cell, CellX, CellY)

    if (Cell.Atoms > 0) then

        if (Cell.Owner == game.TEAM_RED) then
            love.graphics.setColor(255, 0, 0, 255)
        elseif (Cell.Owner == game.TEAM_BLUE) then
            love.graphics.setColor(0, 0, 255, 255)
        end

        for Atom = 1, Cell.Atoms do
            love.graphics.circle("fill", CellX + AtomOffsets[Atom].X, CellY + AtomOffsets[Atom].Y, 10, 100)
        end

        love.graphics.setColor(DEFAULT_COLOR)
    end
end
-- }}}

-- BuildCellPositions {{{
local function BuildCellPositions()
    CellPositions = {};

    for R = 1, FIELD_ROWS do
        CellPositions[R] = {};
        for C = 1, FIELD_COLS do
            local CellX = OFFSET_X + ((C - 1) * CELL_WIDTH)
            local CellY = OFFSET_Y + ((R - 1) * CELL_HEIGHT)
            CellPositions[R][C] = { X = CellX, Y = CellY };       
        end
    end
end
-- }}}

-- WithinRect {{{
local function WithinRect(Rect, Point)
    return (Point.X > Rect.X) and (Point.Y > Rect.Y) and (Point.X < Rect.W) and (Point.Y < Rect.H) 
end
-- }}}

function love.load(Args)
    
    for Arg = 1, #Args do
        if (Args[Arg] == "-H") then
            Host = true
        end
    end

    if (Host) then
        MyTeam = game.TEAM_RED;
        love.window.setTitle(love.window.getTitle() .. "-- HOSTING");
        Connection = socket.udp();
        Connection:setsockname("*", 53474);
        Connection:settimeout(0);

    else
        MyTeam = game.TEAM_BLUE;
        Connection = socket.udp();
        Connection:setpeername("127.0.0.1", 53474);
        Connection:settimeout(0);
        Connection:send("Handshake\n");
    end

    game.Init(FIELD_ROWS, FIELD_COLS);
    BuildCellPositions();
    local ViewFont = love.graphics.newFont("iosevka-regular.ttf", 18);
    love.graphics.setFont(ViewFont);
end

function DrawGameField()
    local Gr = love.graphics;

    Gr.setLineWidth(8)
    Gr.setColor(DEFAULT_COLOR)

    for R = 1, FIELD_ROWS do
        for C = 1, FIELD_COLS do

            local Cell = game.GetCellAt(R, C); 

            local CellX = CellPositions[R][C].X;
            local CellY = CellPositions[R][C].Y;
            if (Cell.Owner ~= game.TEAM_NONE) then
                Gr.setColor(Colors[Cell.Owner]);
                Gr.polygon("fill", CellX, CellY, CellX + CELL_WIDTH, CellY, CellX + CELL_WIDTH, CellY + CELL_HEIGHT, CellX, CellY + CELL_HEIGHT)
                Gr.setColor(DEFAULT_COLOR);
            end

            Gr.line(OFFSET_X + ((C - 1) * CELL_WIDTH), OFFSET_Y, OFFSET_X + ((C - 1) * CELL_HEIGHT), OFFSET_Y + FIELD_HEIGHT)

            DrawAtomsForCell(Cell, CellX, CellY);
            Gr.print(Cell.Atoms, CellX, CellY);
        end

        Gr.line(OFFSET_X, OFFSET_Y + ((R - 1) * CELL_HEIGHT), OFFSET_X + FIELD_WIDTH, OFFSET_Y + ((R - 1) * CELL_HEIGHT))
    end

    Gr.polygon("line", OFFSET_X, OFFSET_Y, OFFSET_X + FIELD_WIDTH, OFFSET_Y, OFFSET_X + FIELD_WIDTH, OFFSET_Y + FIELD_HEIGHT, OFFSET_X, OFFSET_Y + FIELD_HEIGHT)
end


function love.draw()
    if (game.Over) then
        if (game.Winner == TEAM_RED) then
            love.graphics.setColor({255, 64, 64, 255})
            love.graphics.print("RED TEAM WINS", SCREEN_CENTER_X - 50, SCREEN_CENTER_Y - 10);
        elseif (game.Winner == TEAM_BLUE) then
            love.graphics.setColor({64, 64, 255, 255})
            love.graphics.print("BLUE TEAM WINS", SCREEN_CENTER_X - 50, SCREEN_CENTER_Y - 10);
        end
    elseif (not OpponentFound) then
        love.graphics.print("Waiting for opponent", SCREEN_CENTER_X - 70, SCREEN_CENTER_Y - 10);
    else

        if (game.CurrentPlayer() == TEAM_RED) then
            love.graphics.setColor({255, 64, 64, 255})
            love.graphics.print("RED'S TURN", (SCREEN_CENTER_X - 110), 20, 0, 2, 2, 0, 0)
        else
            love.graphics.setColor({64, 64, 255, 255})
            love.graphics.print("BLUE'S TURN", (SCREEN_CENTER_X - 110), 20, 0, 2, 2, 0, 0);
        end

        DrawGameField();
        
        love.graphics.setColor({255, 64, 64, 255})
        love.graphics.print("RED: " .. game.Score[TEAM_RED], 10, 20, 0, 2, 2, 0, 0)

        love.graphics.setColor({64, 64, 255, 255})
        love.graphics.print("BLUE: " .. game.Score[TEAM_BLUE], (1280 - 200), 20, 0, 2, 2, 0, 0)
    end
end


function love.quit()
    Connection:close();
end


local function SendMove(R, C)
    if (Host) then
        local Move = "" .. R .. "," .. C .. "\n";
        Connection:sendto(Move, OpponentIP, OpponentPort);
    else
        local Move = "" .. R .. "," .. C .. "\n";
        Connection:send(Move);
    end
end


local function UpdateGameField()
    for R = 1, FIELD_ROWS do
        for C = 1, FIELD_COLS do
            -- Check for left mouse button click
            if (love.mouse.isDown(1) and game.CurrentPlayer() == MyTeam) then

                local Cell = game.GetCellAt(R, C);

                local CellX = CellPositions[R][C].X;
                local CellY = CellPositions[R][C].Y;

                local CellRect = {
                    X = CellX,
                    Y = CellY,
                    W = CellX + CELL_WIDTH,
                    H = CellY + CELL_HEIGHT
                }

                local MouseXY = { X = love.mouse.getX(), Y = love.mouse.getY() }

                if (WithinRect(CellRect, MouseXY)) then

                    SendMove(R, C);

                    if (Cell.Owner == game.CurrentPlayer() or Cell.Owner == game.TEAM_NONE) then
                        game.PlaceAtomForCurrentPlayer(R, C);
                        game.EndTurnForCurrentPlayer();
                    end

                end

            end
            
        end
    end
end


local function ReceiveMove()
    local Move = Connection:receive();
    if (Move) then
        R, C = Move:match("([^,]+),([^,]+)")
        R = tonumber(R);
        C = tonumber(C);

        local Cell = game.GetCellAt(R, C);
        if (Cell.Owner == game.CurrentPlayer() or Cell.Owner == game.TEAM_NONE) then
            game.PlaceAtomForCurrentPlayer(R, C);
            game.EndTurnForCurrentPlayer();
        end
    end
end


function love.update()
    if (OpponentFound) then
        if (not game.Over) then
            ReceiveMove();
            UpdateGameField();
        end
    else
        OpponentFound = CheckForOpponent();
    end
end


